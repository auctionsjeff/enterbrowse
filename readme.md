# EnterBrowse

EnterBrowse is a simple web browser for the enterprise that is restricted to a handful of URLs. It is intended to be used on kiosks where the user interaction lasts only a handful of seconds for very simple data entry such as checking into an appointment. The goal of the project is to block access to the rest of the machine (even killing explorer.exe if configured to do so) while enabling access to a short list of sites. If enabled, it can even return idle sessions to the home screen. Another goal of the project is a very simple scope and small footprint, so that large enterprise environments with thorough vetting processes can quickly review the code and administrators can be confident the app isn't malicious.

## Getting Started

Here are some instructions to get the app setup and running on your kiosk.

### Prerequisites

This app requires .NET Framework 3.5, which is included in Windows 7, 8, 8.1, and 10, and can be installed on Windows Vista.

### Installing

Simply place the EnterBrowse.exe and the EnterBrowse.exe.config files in a folder on the kiosk. Ensure they have permissions to execute under the user that will be signed-in to the kiosk.

### Gotchas

The app includes very primitive countermeasures that prevent normal users from closing it, or accessing the underlying system. It is told to be "Always On Top" of other programs, and has no standard close button. To close the app, double-click the title text at the top (which defaults to a message about a touchscreen) and enter the configured "Close Command" (which by default is "closeme"). Because apps that aggressively fight the user's attempts to close them seem malicious, this app does not prevent the user from opening the Task Manager, or any other more sophisticated method of ending the process, though it can be configured to attempt to end the "explorer.exe" task when the app launches (to simplify startup scripts).

Also, keep in mind that to reduce the app's footprint, the default WebBrowser control is used, which is roughly equivalent in functionality to Internet Explorer 6. This will likely impact the usability of the websites used in the app.

### Configuration

* **TitleMessage** - the message to show at the top of the window. If it is too long it will get cut off... too short, and you can't close the app (nowhere to double-click)
* **BackgroundRGB** - the color for the top/bottom bars. Default is gray. Values are R, G, B. Use this color picker: https://www.webpagefx.com/web-design/color-picker/
* **HomeURL** - where the "house" icon navigates to, and where the app starts out at and resets to. Should be your volunteer login page.
* **DogURL** - the other URL you want users to have easy access to.
* **TopMost** - if True, the app will cover up all other apps, the same way Task Manager does. Useful to prevent Alt+Tab. Defaults to True.
* **CloseCommand** - this is that special word/phrase you type so that normal users can't close the app. Remember to double-click the title message to be prompted for this phrase.
* **MessageColorRGB** - similar to background, but for the Title Message text. defaults to black (0,0,0)
* **ResetOnIdle** - if the user navigates to a page and is idle, this will log them out and reset to the home page if enabled.​​
* **IdleTimeoutSec** - see ResetOnIdle. This controls how long to wait after no mouse/keyboard input before resetting the browser to the home page. Default is 60 seconds.
* **SpaHomeID** - for web pages that don't navigate (such as Single Page Apps, SPA), look for an HTML element with this ID, and consider the page to be navigated when this element exists but is not visible.
* **KillExplorer** - (requires admin) if True, this attempts to close "explorer.exe" when the app starts. Note: modern versions of Windows will try to start it again. You can disable your shell in the registry: https://superuser.com/questions/435275/can-you-disable-windows-explorer-from-starting-with-windows
* **DogFormLoadedID** - detects if the form loaded correctly based on the existence of an element with this ID. If missing, it will go through the login workflow automatically
* **DogLoginUserNameField** - the field's ID on the login page of the dog website
* **DogLoginPasswordNameField** - the field's ID on the login page of the dog website
* **DogAutoLoginUser** - the actual user name to fill in the login form
* **DogAutoLoginPass** - the actual password to fill in the login form
* **DogAuthPage** - the page to go to for signing into the dog website

**Note:** RGB configured options can also use color names instead of RGB values.

## Deployment

 * Customize the config file to suit your needs.
 * Create a startup script for the kiosk (Group Policy), or make a registry entry that will start the app on startup.

## Updating

For security reasons, the app cannot update itself. To update it, close the app and redeploy new binaries manually. Keep in mind the config file will likely change between versions, so make sure to merge your customizations into the new config file, and use the matching version of config file when redeploying the app.

## License

This project is licensed under the MIT License - see the [license.md](license.md) file for details.