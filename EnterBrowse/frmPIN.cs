﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EnterBrowse
{
    public partial class frmPIN : Form
    {
        private string _correctPIN = "";
        private string _inputSoFar = "";

        public frmPIN(string correctPIN)
        {
            InitializeComponent();
            _correctPIN = correctPIN.ToUpper();
        }

        private void button_Click(object sender, EventArgs e)
        {
            _inputSoFar += ((Button)sender).Tag;

            if (_inputSoFar.Length > _correctPIN.Length)
            {
                // bail!
                MessageBox.Show("Incorrect PIN");
                DialogResult = DialogResult.Abort;
            }

            if (_inputSoFar == _correctPIN)
            {
                DialogResult = DialogResult.OK;
            }
        }
    }
}
