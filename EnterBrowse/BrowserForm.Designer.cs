﻿namespace EnterBrowse
{
    partial class BrowserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BrowserForm));
            this.browser = new System.Windows.Forms.WebBrowser();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblMessages = new System.Windows.Forms.Label();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnHome = new System.Windows.Forms.ToolStripButton();
            this.btnDog = new System.Windows.Forms.ToolStripButton();
            this.IdleTimer = new System.Windows.Forms.Timer(this.components);
            this.tmrAJAX = new System.Windows.Forms.Timer(this.components);
            this.tmrSendKeys = new System.Windows.Forms.Timer(this.components);
            this.pnlTop.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // browser
            // 
            this.browser.AllowWebBrowserDrop = false;
            this.browser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browser.IsWebBrowserContextMenuEnabled = false;
            this.browser.Location = new System.Drawing.Point(0, 52);
            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.browser.Name = "browser";
            this.browser.ScriptErrorsSuppressed = true;
            this.browser.Size = new System.Drawing.Size(800, 298);
            this.browser.TabIndex = 0;
            this.browser.Url = new System.Uri("http://www.google.com", System.UriKind.Absolute);
            this.browser.WebBrowserShortcutsEnabled = false;
            this.browser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.browser_DocumentCompleted);
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.lblMessages);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(800, 52);
            this.pnlTop.TabIndex = 1;
            // 
            // lblMessages
            // 
            this.lblMessages.BackColor = System.Drawing.Color.Gray;
            this.lblMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMessages.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessages.Location = new System.Drawing.Point(0, 0);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(800, 52);
            this.lblMessages.TabIndex = 0;
            this.lblMessages.Text = "This monitor is not a touch screen. Please use the mouse and keyboard below.";
            this.lblMessages.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMessages.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lblMessages_MouseDoubleClick);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.toolStrip1);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 350);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(800, 100);
            this.pnlBottom.TabIndex = 2;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Gray;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(96, 96);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnHome,
            this.btnDog});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 103);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // btnHome
            // 
            this.btnHome.AutoToolTip = false;
            this.btnHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHome.Image")));
            this.btnHome.ImageTransparentColor = System.Drawing.Color.White;
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(100, 100);
            this.btnHome.Text = "btnHom";
            // 
            // btnDog
            // 
            this.btnDog.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnDog.AutoToolTip = false;
            this.btnDog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDog.Image = ((System.Drawing.Image)(resources.GetObject("btnDog.Image")));
            this.btnDog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDog.Name = "btnDog";
            this.btnDog.Size = new System.Drawing.Size(100, 100);
            this.btnDog.Text = "toolStripButton2";
            // 
            // IdleTimer
            // 
            this.IdleTimer.Enabled = true;
            this.IdleTimer.Interval = 1000;
            this.IdleTimer.Tick += new System.EventHandler(this.IdleTimer_Tick);
            // 
            // tmrAJAX
            // 
            this.tmrAJAX.Enabled = true;
            this.tmrAJAX.Interval = 1000;
            // 
            // tmrSendKeys
            // 
            this.tmrSendKeys.Interval = 1200;
            this.tmrSendKeys.Tick += new System.EventHandler(this.tmrSendKeys_Tick);
            // 
            // BrowserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.browser);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.pnlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BrowserForm";
            this.Text = "Form1";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pnlTop.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser browser;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Timer IdleTimer;
        private System.Windows.Forms.Timer tmrAJAX;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnHome;
        private System.Windows.Forms.ToolStripButton btnDog;
        private System.Windows.Forms.Timer tmrSendKeys;
    }
}

