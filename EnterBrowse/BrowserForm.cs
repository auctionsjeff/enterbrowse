﻿using Microsoft.VisualBasic;
using System;
using System.Windows.Forms;

namespace EnterBrowse
{
    public partial class BrowserForm : Form
    {
        private string HomeURL = "";
        private string DogURL = "";
        private string SpaHomeID = "";

        // this enum is used to signify the Navigation "intent". used in DocumentCompleted event to take action based on intent
        private enum LoginStep
        {
            None = 0,
            GoHome = 1,
            GoDog = 2,
            LoginPage = 3,
            AfterLogin = 4
        }

        private bool HasNavigatedSinceReset = false;
        private DateTime LastReset = DateTime.Now;
        private LoginStep NavigationStep = LoginStep.GoHome;
        private int IdleSec = 0;

        public BrowserForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            
            HomeURL = Properties.Settings.Default.HomeURL;
            DogURL = Properties.Settings.Default.DogURL;
            string BackgroundRGB = Properties.Settings.Default.BackgroundRGB;
            SpaHomeID = Properties.Settings.Default.SpaHomeID;

            if (BackgroundRGB.Contains(","))
            {
                // assume it's RGB
                string[] parts = BackgroundRGB.Split(',');
                pnlTop.BackColor = System.Drawing.Color.FromArgb(255, int.Parse(parts[0].Trim()), int.Parse(parts[1].Trim()), int.Parse(parts[2].Trim()));
            }
            else
            {
                // assume it's a known color name
                pnlTop.BackColor = System.Drawing.Color.FromName(BackgroundRGB);
            }
            string MessageColorRGB = Properties.Settings.Default.MessageColorRGB;
            if (MessageColorRGB.Contains(","))
            {
                // assume it's RGB
                string[] parts = MessageColorRGB.Split(',');
                lblMessages.ForeColor = System.Drawing.Color.FromArgb(255, int.Parse(parts[0].Trim()), int.Parse(parts[1].Trim()), int.Parse(parts[2].Trim()));
            }
            else
            {
                // assume it's a known color name
                lblMessages.ForeColor = System.Drawing.Color.FromName(MessageColorRGB);
            }
            pnlBottom.BackColor = pnlTop.BackColor;
            toolStrip1.BackColor = pnlBottom.BackColor;
            lblMessages.Text = Properties.Settings.Default.TitleMessage;
            lblMessages.BackColor = pnlTop.BackColor;
            WipeScreenAndNavigate(HomeURL);

            if (!Properties.Settings.Default.ResetOnIdle)
            {
                IdleTimer.Enabled = false;
            }
            else
            {
                IdleSec = Properties.Settings.Default.IdleTimeoutSec;
            }

            // if configured, look for and kill "explorer.exe"
            if (Properties.Settings.Default.KillExplorer)
            {
                Utilities.KillProcess("explorer");
            }
        }

        private void WipeScreenAndNavigate(string URL)
        {
            // this improves the visual experience by clearing out the old page so it appears to be "doing something" after the user clicks the home/dog button.
            browser.Navigate("about:blank");
            browser.Document.Write(".");
            browser.Navigate(URL);
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "btnDog":
                    // add password protection
                    TopMost = false;    // hide us so the dialog can show
                    frmPIN pin = new frmPIN(Properties.Settings.Default.DogAccessPIN);
                    if (pin.ShowDialog() != DialogResult.OK)
                    {
                        TopMost = true;
                        return;
                    }
                    TopMost = true;
                    NavigationStep = LoginStep.GoDog;
                    WipeScreenAndNavigate(DogURL);
                    break;
                case "btnHome":
                    NavigationStep = LoginStep.GoHome;
                    WipeScreenAndNavigate(HomeURL);
                    break;
            }
        }

        private void IdleTimer_Tick(object sender, EventArgs e)
        {
            // if SPA ID is defined, check for element with that ID, and if invisible, consider it a non-home page
            if (SpaHomeID.Length > 0)
            {
                HtmlElement targetElement = browser.Document.All[SpaHomeID];
                if (targetElement != null && targetElement.Style.ToLower().Contains("display: none"))
                {
                    HasNavigatedSinceReset = true;
                }
            }

            // if mouse has been idle for more than X seconds, reset to home page
            if (!HasNavigatedSinceReset)
            {
                // no point in resetting unless the user has browsed away
                return;
            }
            if (DateTime.Now.Subtract(LastReset).TotalSeconds < IdleSec)
            {
                // no point in checking system idle time unless it's been at least X sec since we last reset
                Console.WriteLine("last reset was " + DateTime.Now.Subtract(LastReset).TotalSeconds);
                return;
            }
            int idleSeconds = 0;
            try
            {
                long rawValue = Utilities.GetIdleTickCount();
                idleSeconds = (int)rawValue / 1000;
            }
            catch (Exception ex)
            {
                // hmm, treat idle as empty
                idleSeconds = 0;
            }
            if (idleSeconds >= IdleSec)
            {
                Reset();
            } else
            {
                Console.WriteLine("idle = " + idleSeconds);
            }
        }

        private void Reset()
        {
            LastReset = DateTime.Now;
            HasNavigatedSinceReset = false;
            NavigationStep = LoginStep.GoHome;
            browser.Navigate(HomeURL);
        }

        private void lblMessages_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            TopMost = false; // we have to be non-topmost to allow inputbox to show
            string input = Interaction.InputBox("Enter command:", "Kiosk Mode");
            if (input.ToLower() == Properties.Settings.Default.CloseCommand || input == Properties.Settings.Default.CloseCommand)
            {
                this.Close();
            }
            else
            {
                MessageBox.Show("The command was not understood.", "Kiosk Mode");
            }
            TopMost = Properties.Settings.Default.TopMost;
        }

        private void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (NavigationStep == LoginStep.None)
            {
                HasNavigatedSinceReset = true;
            }
            else if (NavigationStep == LoginStep.GoHome)
            {
                // we were told to go home by reset process, so reset flag
                HasNavigatedSinceReset = false;
                NavigationStep = LoginStep.None;
            } else if (NavigationStep == LoginStep.GoDog)
            {
                // make sure the real form displayed, not the Not Logged In (home) Page
                if (browser.Document.All[Properties.Settings.Default.DogFormLoadedID] == null) {
                    // do the login flow
                    NavigationStep = LoginStep.LoginPage;
                    WipeScreenAndNavigate(Properties.Settings.Default.DogAuthPage);
                    return;
                }
                // otherwise, form is shown and we are done!
                NavigationStep = LoginStep.None;
                HasNavigatedSinceReset = true;
            } else if (NavigationStep == LoginStep.LoginPage)
            {
                // stay on this step until we see the form
                // find the user/pass fields and enter the data
                HtmlElement loginForm = browser.Document.All["login"];
                if (loginForm != null)
                {
                    HtmlElement userField = browser.Document.All[Properties.Settings.Default.DogLoginUserNameField];
                    HtmlElement passField = browser.Document.All[Properties.Settings.Default.DogLoginPasswordField];

                    userField.SetAttribute("value", Properties.Settings.Default.DogAutoLoginUser);
                    passField.SetAttribute("value", Properties.Settings.Default.DogAutoLoginPass);
                    NavigationStep = LoginStep.AfterLogin;

                    // this handler must return before the form will render the page...
                    tmrSendKeys.Enabled = true;
                    tmrSendKeys.Tag = passField;
                }
            } else if (NavigationStep == LoginStep.AfterLogin)
            {
                NavigationStep = LoginStep.GoDog;
                HasNavigatedSinceReset = true;
                WipeScreenAndNavigate(DogURL);
            }
        }

        private void tmrSendKeys_Tick(object sender, EventArgs e)
        {
            tmrSendKeys.Enabled = false;
            HtmlElement passField = (HtmlElement)tmrSendKeys.Tag;
            passField.Focus();
            SendKeys.Send("{enter}");
        }
    }
}

